const filmsContainer = document.getElementById("films");

//
fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((films) => {
    //
    films.forEach((film) => {
      const filmElement = document.createElement("div");
      filmElement.innerHTML = `
        <h2>Episode ${film.episodeId}: ${film.name}</h2>
        <p>${film.openingCrawl}</p>
      `;
      filmsContainer.appendChild(filmElement);

      //
      const loaderElement = document.createElement("div");
      loaderElement.classList.add("loader");
      filmElement.appendChild(loaderElement);

      //
      Promise.all(
        film.characters.map((characterUrl) =>
          fetch(characterUrl).then((response) => response.json())
        )
      ).then((characters) => {
        //
        loaderElement.remove();
        const characterListElement = document.createElement("ul");
        characters.forEach((character) => {
          const characterItemElement = document.createElement("li");
          characterItemElement.textContent = character.name;
          characterListElement.appendChild(characterItemElement);
        });
        filmElement.appendChild(characterListElement);
      });
    });
  });
